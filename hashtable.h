#ifndef __HASHTABLE_H__
#define __HASHTABLE_H__

void *hinit(void);
void hinsert(void *handle, int value);
int hdelete(void *handle, int value);
int hsearch(void *handle, int value);
void hfinit(void *handle);

#endif
