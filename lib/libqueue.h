#ifndef __LIBQUEUE_H__
#define __LIBQUEUE_H__

void qput(void *n);
void *qget(void);
void qinit(void);
void qfinit(int clear);
int qisempty(void);

#endif
