#ifndef __BINARYTREE_H__
#define __BINARYTREE_H__

void *binit(void);
void bfinit(void *handle);
int binsert(void *handle, int val);
int bdelete(void *handle, int val);
void bpreorder(void *handle);
void binorder(void *handle);
void bpostorder(void *handle);
void blevelorder(void *handle);

#endif
