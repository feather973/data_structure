#include <stdio.h>

#define QMAX		100
int queue[QMAX];

/* 
 * ex) put 1 2 3 4
 *
 * rear       front
 *  4   3   2   1
 *
 * enter data to rear
 * get data from front
 */ 
int rear, front;

#include "arrayqueue.h"

void aqinit(void)
{
	front = rear = 0;
}

void aqput(int val)
{
	queue[rear] = val;
	if (rear == QMAX - 1)
		rear = 0;
	else
		rear++;
}

int aqget(void)
{
	int ret;
	ret = queue[front];

	if (front == QMAX -1)
		front = 0;
	else
		front++;

	return ret;
}

void aqtrav(void)
{
	int i;

	for (i=0;i<QMAX;i++) {
		printf("%d ", queue[i]);

		if ((i % 10 == 9))
			printf("\n");
	}
}

void aqfinit(void)
{
	return;	
}
