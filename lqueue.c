#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

typedef struct _lqnode {
	int val;
	struct _lqnode *next;
} lqnode;

/*
 * IMPORTANT
 * ex) put a b c d
 *
 * front           rear
 *   a    b    c    d
 *
 * add to rear->next ( rear->next points rear's previous node )
 * get from front
 */
lqnode *lqfront, *lqrear;

#include "lqueue.h"

void lqinit(void)
{
	lqfront = malloc(sizeof(lqnode));
	if (!lqfront) {
		perror("lqinit");
		return;
	}

	lqrear = malloc(sizeof(lqnode));
	if (!lqrear) {
		perror("lqinit");
		free(lqfront);
		return;
	}

	lqfront->next = lqrear;	
	lqrear->next = lqrear;
}

void lqput(int val)
{
	lqnode *ptr;

	ptr = malloc(sizeof(lqnode));
	if (!ptr) {
		perror("lqput");
		return;
	}

	ptr->val = val;
	
	if (lqfront->next == lqrear) {
		ptr->next = lqrear;
		lqfront->next = ptr;

		lqrear->next = ptr;
	} else {
		lqrear->next->next = ptr; 
		ptr->next = lqrear;

		lqrear->next = ptr;
	}
}

int lqget(void)
{
	lqnode *remove;
	int ret;

	if (lqfront->next == lqrear) {
		fprintf(stdout, "queue empty\n");
		return -1;
	}

	ret = lqfront->val;
	remove = lqfront;
	
	lqfront = lqfront->next;
	free(remove);

	return ret;
}

void lqtrav(void)
{
	lqnode *ptr = lqfront->next;
	int i = 0;

	while (ptr != lqrear) {
		printf("%d ", ptr->val);

		if (i % 10 == 9)
			printf("\n");

		ptr = ptr->next;
		i++;
	}
}

void lqfinit(void)
{
	lqnode *ptr = lqfront;

	while (ptr != lqrear) {
		// update front, free old front
		lqfront = lqfront->next;
		free(ptr);

		ptr = lqfront;
	}

	// free end
	free(ptr);
}
