#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

typedef struct _cnode {
	int val;
	struct _cnode *prev;
	struct _cnode *next;
} cnode;

cnode *cur;

#include "circular.h"

void cinit(void)
{
	return;
}

void cfinit(void)
{
	cnode *start, *remove;
	int i;
	
	if (!cur)
		return;

	start = cur;

	do {
		remove = cur;
		cur = cur->next;

		free(remove);
	} while (cur != start);
}

void cinsert(int val) {
	cnode *n;

	n = malloc(sizeof(cnode));
	if (!n)
		return;

	n->val = val;
	n->next = NULL;
	
	if (!cur) {
		// make it circular
		n->next = n;
		n->prev = n;

		cur = n;
	} else {
		// link of n
		n->next = cur->next;
		n->prev = cur; 

		// link of cur
		cur->next = n;

		// update cur
		cur = n;
	}

	return;
}

int cdelete(void) {
	cnode *remove;
	int val;

	if (!cur) {
		perror("cdelete");
		return -1;
	}

	remove = cur;
	// remove link
	cur->prev->next = cur->next;
	cur->next->prev = cur->prev;

	// update cur
	cur = cur->next;
	val = remove->val;

	free(remove);

	return val;
}

void ctrav(void) {
	cnode *ptr, *start;
	int i = 0;

	ptr = start = cur;
	do {
		if (!ptr)
			break;

		printf("%d ", ptr->val);
		i++;

		if (i % 10 == 0)
			printf("\n");

		ptr = ptr->next;
	} while (ptr != start);
}
