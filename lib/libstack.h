#ifndef __LIBSTACK_H__
#define __LIBSTACK_H__

void *sinit(void);
void sfinit(void *handle, int clear);

void spush(void *handle, void *n);
void *spop(void *handle);

int sisempty(void *handle);

#endif
