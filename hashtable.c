// using key-based addressing and separate chaining

#include <sys/queue.h>
#include <stddef.h>		// NULL
#include <stdio.h>		// fprintf
#include <stdlib.h>		// calloc
#include <errno.h>		// errno
#include <string.h>		// strerror

struct _hnode {
	int value;
	struct _hnode *next;
};
typedef struct _hnode hnode;

#define HASH_TABLE_SIZE		100
struct _hashbucket {
	struct _hnode *first;
};
typedef struct _hashbucket hashbucket;
struct _hashinfo {
	hashbucket buckets[HASH_TABLE_SIZE];
};
typedef struct _hashinfo hashinfo;

/* since value is int, there is no need to use complex hash function */
static int hgethash(int value) {
	return value;
}

static hashbucket *hgetbucket(hashinfo *h, int hash) {
	return &h->buckets[hash % HASH_TABLE_SIZE];
}

void* hinit(void) {
	hashinfo *h = calloc(1, sizeof(hashinfo));
	if (!h) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	return h;
}

void hfinit(void *handle) {
	int i, j;
	hashinfo *h;
	hashbucket *head;
	hnode *cur, *next;

	h = (hashinfo *) handle;

	for (i=0;i<HASH_TABLE_SIZE;i++) {
		head = &h->buckets[i];

		cur = head->first;
		while (cur) {
			next = cur;
			free(cur);
			cur = next;
		}
	}
}

void hinsert(void *handle, int value) {
	hashinfo *h = (hashinfo *) handle;
	hashbucket *head;
	int hash;
	hnode *n, *cur, *next;

	n = calloc(1, sizeof(hnode));
	if (!n) {
		fprintf(stdout, "[%s(%d)] calloc fail\n", __func__ , __LINE__);
		return;
	}
	n->value = value;

	hash = hgethash(value);
	head = hgetbucket(h, hash);

	if (!head->first) {			// boundary condition
		head->first = n;
	} else {
		cur = next = head->first;
		while (next) {
			cur = next;
			next = next->next;
		}

		cur->next = n;
	}

	printf("[INSERT] %d\n", value);
}

int hdelete(void *handle, int value) {
	hashinfo *h = (hashinfo *) handle;
	hashbucket *head;
	int hash;
	hnode *cur, *before;
	int ret = 0;

	hash = hgethash(value);
	head = hgetbucket(h, hash);

	cur = before = head->first;

	while (cur) {
		if (cur->value == value) {
			if (cur->next) {
				if (before == cur)
					head->first = cur->next;
				else
					before->next = cur->next;

				free(cur);
			} else {
				if (before == cur)
					head->first = NULL;
				else
					before->next = NULL;

				free(cur);
			}
			printf("[DELETE] %d\n", value);
			ret = 1;
			break;
		}

		before = cur;
		cur = cur->next;
	}

	return ret;
}

int hsearch(void *handle, int value) {
	hashinfo *h = (hashinfo *) handle;
	hashbucket *head;
	int hash;
	hnode *cur;
	int ret = 0;

	hash = hgethash(value);
	head = hgetbucket(h, hash);

	cur = head->first;
	while (cur) {
		if (cur->value == value) {
			printf("[SEARCH] %d\n", value);	
			ret = 1;
			break;
		}

		cur = cur->next;
	}

	return ret;
}
