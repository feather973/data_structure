#ifndef __RBTREE_H__
#define __RBTREE_H__

void *rbinit(void);
void rbfinit(void *handle);
int rbinsert(void *handle, int val);
int rbdelete(void *handle, int val);
#if 0
void rbpreorder(void *handle);
void rbinorder(void *handle);
void rbpostorder(void *handle);
void rblevelorder(void *handle);
#endif

void removeall(void *handle);

#endif
