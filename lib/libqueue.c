#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

typedef struct _qnode {
	void *n;
	struct _qnode *next;
} qnode;

// put front
// get rear
static qnode *front, *rear;

#include "libqueue.h"
/*
 * rear -- front
 */
void qinit(void) {
	front = calloc(1, sizeof(qnode));
	if (!front) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return;
	}

	rear = calloc(1, sizeof(qnode));
	if (!rear) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return;
	}

	front->next = rear;
	rear->next = rear;
}

void qfinit(int clear) {
	qnode *tmp;

	while (front != rear) {
		tmp = front->next;
		if (clear)
			free(front->n);
		free(front);
		front = tmp;
	}

	front = NULL;

	free(rear);
	rear = NULL;
}

/*
 * put a b c
 *
 * rear -- a -- front    (front->next is a,  a->next is rear,  "rear->next is a")
 * rear -- b -- a -- front
 * rear -- c -- b -- a -- front
 */
void qput(void *n) {
	qnode *ptr;

	ptr = calloc(1, sizeof(qnode));
	if (!ptr) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return;
	}

	ptr->n = n;

	if (front->next == rear) {
		front->next = ptr;
		ptr->next = rear;
		rear->next = ptr;
	} else {
		rear->next->next = ptr;		// key point
		ptr->next = rear;
		rear->next = ptr;
	}
}

/*
 * get get get
 *
 * rear -- c -- b -- front
 * rear -- c -- front
 * rear -- front
 */
void *qget(void) {
	qnode *remove;
	void *ret;

	if (front->next == rear) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	remove = front->next;

	front->next = front->next->next;
	ret = remove->n;

	free(remove);	

	return ret;	
}

int qisempty(void) {
	if (front->next == rear) {
		return 1;
	}

	return 0;
}
