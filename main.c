#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

#include "circular.h"
#include "arrayqueue.h"
#include "lib/libstack.h"
#include "lib/libqueue.h"
#include "binarytree.h"
#include "avl.h"
#include "priorityqueue.h"
#include "hashtable.h"
#include "rbtree.h"

void help(void)
{
	fprintf(stdout, "==== usage ====\n");
	fprintf(stdout, " h : help\n");	
	fprintf(stdout, " t : test TEST_TARGET\n");	
	fprintf(stdout, "  circular : circular list test\n");	
	fprintf(stdout, "  arrayqueue : array queue test\n");	
	fprintf(stdout, "  stack : stack test\n");	
	fprintf(stdout, "  queue : queue test\n");	
	fprintf(stdout, "  binarytree : binary tree test\n");	
	fprintf(stdout, "  avltree : avl tree test\n");	
	fprintf(stdout, "  priorityqueue : priority queue test\n");
	fprintf(stdout, "  hashtable : hash table test\n");
	fprintf(stdout, "  rbtree : rbtree test\n");
	fprintf(stdout, "  rbtree2 : rbdelete test\n");
	fprintf(stdout, "  rbtree3 : rbdelete random test\n");
}

static void rbtree_test(void)
{
	int i, n;
	bool is_insert_complete;

	int inserttest[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	int inserted[sizeof(inserttest)/sizeof(int)] = {0,};

	void *handle;
	handle = rbinit();

	srand(time(NULL));
	memset(inserted, 0, sizeof(inserted)/sizeof(int));

	while (1) {
		// make index n
		while (1) {
			n = rand() % (sizeof(inserttest)/sizeof(int));
			if (inserted[n] == 0) {
				inserted[n] = 1;
				break;
			}
		}

		// insert
		printf("insert %d\n", inserttest[n]);
		rbinsert(handle, inserttest[n]);

		// end condition check
		is_insert_complete = true;

		for (i=0;i<sizeof(inserted)/sizeof(int);i++) {
			if (inserted[i] == 0) {
				is_insert_complete = false;
				break;
			}
		}

		// end
		if (is_insert_complete)
			break;
	}

	removeall(handle);

	rbfinit(handle);

	return;
}

// test for manual delete
static void rbtree_test2(void)
{
	int i, n;
	bool is_insert_complete;

//	int inserttest[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
//	int inserttest[] = {20, 40, 90, 100, 80, 50, 70, 30, 10, 60};
	int inserttest[] = {80, 90, 10, 50, 60, 100, 70, 20, 40, 30};

	void *handle;
	handle = rbinit();

	for (i=0;i<sizeof(inserttest)/sizeof(int);i++) {
		printf("insert %d\n", inserttest[i]);
		rbinsert(handle, inserttest[i]);
	}

#if 0
	rbdelete(handle, 10);
	rbdelete(handle, 20);
	rbdelete(handle, 30);
	rbdelete(handle, 40);
	rbdelete(handle, 50);
	rbdelete(handle, 60);
	rbdelete(handle, 70);
	rbdelete(handle, 80);
	rbdelete(handle, 90);
	rbdelete(handle, 100);
#endif

#if 0
	rbdelete(handle, 70);
	rbdelete(handle, 80);
	rbdelete(handle, 30);
	rbdelete(handle, 50);
	rbdelete(handle, 40);
	rbdelete(handle, 60);
	rbdelete(handle, 10);
	rbdelete(handle, 20);
	rbdelete(handle, 90);
	rbdelete(handle, 100);
#endif
	rbdelete(handle, 60);

	removeall(handle);

	rbfinit(handle);

	return;
}

// test for random delete
static void rbtree_test3(void)
{
	int i, n;
	bool is_insert_complete;
	bool is_delete_complete;

	int inserttest[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	int inserted[sizeof(inserttest)/sizeof(int)] = {0,};

	int deletetest[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	int deleted[sizeof(deletetest)/sizeof(int)] = {0,};

	void *handle;
	handle = rbinit();

	srand(time(NULL));
	memset(inserted, 0, sizeof(inserted)/sizeof(int));
	memset(deleted, 0, sizeof(inserted)/sizeof(int));

	while (1) {
		// make index n
		while (1) {
			n = rand() % (sizeof(inserttest)/sizeof(int));
			if (inserted[n] == 0) {
				inserted[n] = 1;
				break;
			}
		}

		// insert
		printf("insert %d\n", inserttest[n]);
		rbinsert(handle, inserttest[n]);

		// end condition check
		is_insert_complete = true;

		for (i=0;i<sizeof(inserted)/sizeof(int);i++) {
			if (inserted[i] == 0) {
				is_insert_complete = false;
				break;
			}
		}

		// end
		if (is_insert_complete)
			break;
	}

	while (1) {
		// make index n
		while (1) {
			n = rand() % (sizeof(deletetest)/sizeof(int));
			if (deleted[n] == 0) {
				deleted[n] = 1;
				break;
			}
		}

		// delete
		printf("delete %d\n", deletetest[n]);
		rbdelete(handle, deletetest[n]);

		// end condition check
		is_delete_complete = true;

		for (i=0;i<sizeof(deleted)/sizeof(int);i++) {
			if (deleted[i] == 0) {
				is_delete_complete = false;
				break;
			}
		}

		// end
		if (is_delete_complete)
			break;
	}

	removeall(handle);

	rbfinit(handle);

	return;
}


#define HASH_TABLE_TEST_DATA_SIZE		10000
static void hash_table_test(void)
{
	void *handle;
	int ret, i;

	handle = hinit();

	srand(time(NULL));

	for (i=0;i<HASH_TABLE_TEST_DATA_SIZE;i++) {
		hinsert(handle, i);
	}

	ret = hsearch(handle, rand() % HASH_TABLE_TEST_DATA_SIZE);
	if (!ret) {
		fprintf(stdout, "[hsearch] fail to search %d", i);
		goto end;
	}

	for (i=HASH_TABLE_TEST_DATA_SIZE-1;i>=0;i--) {
		ret = hdelete(handle, i);
		if (!ret) {
			fprintf(stdout, "[hdelete] fail to delete %d", i);
			goto end;
		}
	}

end:
	hfinit(handle);
	return;
}

static void priority_queue_test(void)
{
	int i, n;
	int puttest[50];

	void *handle;
	handle = pinit();

	srand(time(NULL));

	for (i=0;i<sizeof(puttest)/sizeof(int);i++) {
		n = rand() % 99999;
		puttest[i] = n;
	}

	for (i=0;i<sizeof(puttest)/sizeof(int);i++) {
		pput(handle, puttest[i]);
		usleep(100000);			// 100ms
	}

	for (i=0;i<sizeof(puttest)/sizeof(int);i++) {
		printf("%d\n", pget(handle));
		usleep(100000);			// 100ms
	}

	pfinit(handle);
}

static void avl_random_test(void)
{
	int i, n;

	int inserttest[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	int inserted[sizeof(inserttest)/sizeof(int)] = {0,};
	bool is_insert_complete = true;

	int removetest[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	
	void *handle;
	handle = ainit();

	srand(time(NULL));
	memset(inserted, 0, sizeof(inserted)/sizeof(int));

	while (1) {
		// make index n
		while (1) {
			n = rand() % (sizeof(inserttest)/sizeof(int));
			if (inserted[n] == 0) {
				inserted[n] = 1;
				break;
			}
		}

		// insert
		ainsert(handle, inserttest[n]);
		alevelorder(handle);

		// end condition check
		is_insert_complete = true;

		for (i=0;i<sizeof(inserted)/sizeof(int);i++) {
			if (inserted[i] == 0) {
				is_insert_complete = false;
				break;
			}
		}
		
		// end
		if (is_insert_complete)
			break;
	}

	fprintf(stdout, "==== avl tree preorder ====\n");
	sleep(1);
	apreorder(handle);
	fprintf(stdout, "==== avl tree inorder ====\n");
	ainorder(handle);
	fprintf(stdout, "==== avl tree postorder ====\n");
	apostorder(handle);
	fprintf(stdout, "==== avl tree levelorder ====\n");
	alevelorder(handle);

	fprintf(stdout, "==== avl tree remove test ====\n");
	sleep(1);

	for (i=0;i<sizeof(removetest)/sizeof(int);i++) {
		printf("remove %d : ", removetest[i]);
		adelete(handle, removetest[i]);
		alevelorder(handle);
	}

	afinit(handle);
}

void avl_tree_test(void)
{
	int i;
	
	for (i=0;i<10;i++)
		avl_random_test();

	return;
}

void binary_tree_test(void)
{
	int i;
	int inserttest[] = {10, 20, 1, 30, 2, 300, 40};
	int removetest[] = {2, 30, 1, 300, 10, 40, 20};

	void *handle;
	handle = binit();

	for (i=0;i<sizeof(inserttest)/sizeof(int);i++) {
		binsert(handle, inserttest[i]);
	}

	fprintf(stdout, "==== binary tree preorder ====\n");
	bpreorder(handle);
	fprintf(stdout, "==== binary tree inorder ====\n");
	binorder(handle);
	fprintf(stdout, "==== binary tree postorder ====\n");
	bpostorder(handle);
	fprintf(stdout, "==== binary tree levelorder ====\n");
	blevelorder(handle);

	fprintf(stdout, "==== binary tree remove test ====\n");

	for (i=0;i<sizeof(removetest)/sizeof(int);i++) {
		printf("remove %d : ", removetest[i]);
		bdelete(handle, removetest[i]);
		blevelorder(handle);
	}

	bfinit(handle);
}

void queue_test(void)
{
	int i;
	int testarr[20];

	for (i=0;i<20;i++) {
		testarr[i] = i*100;
	}

	qinit();

	for (i=0;i<20;i++) {
		qput(testarr+i);
	}

	for (i=0;i<20;i++) {
		printf("%d\n", *(int *)qget());
	}

	qfinit(1);
	return;
}

void arrayqueue_test(void)
{
	int i;
	aqinit();
	for (i=0;i<110;i++) {
		aqput(i);
	}

	aqtrav();

	for (i=0;i<100;i++) {
		aqget();
	}

	aqfinit();
	return;
}

void stack_test(void)
{
	int i;
	int testarr[20];
	void *handle;

	for (i=0;i<20;i++) {
		testarr[i] = i*1000;
	}

	handle = sinit();

	for (i=0;i<20;i++) {
		spush(handle, testarr+i);
	}

	for (i=0;i<20;i++) {
		printf("%d\n", *(int *)spop(handle));
	}

	sfinit(handle, 1);
	return;
}

void circular_test(void)
{
	int i;
	cinit();
	for (i=0;i<100;i++) {
		cinsert(i*100);
	}

	ctrav();

	for (i=0;i<100;i++) {
		cdelete();
	}
	
	cfinit();
	return;
}

int main(int argc, char *argv[])
{
	int c;
	char *test;

	if (argc == 1) {
		help();
		return 1;
	}

	while ((c = getopt(argc, argv, "ht:")) >= 0) {
		switch (c) {
		case 'h':
			help();
			break;
		case 't':
			test = optarg;
			if (!strcmp(test, "circular")) {
				circular_test();
			} else if (!strcmp(test, "arrayqueue")) {
				arrayqueue_test();
			} else if (!strcmp(test, "stack")) {
				stack_test();
			} else if (!strcmp(test, "queue")) {
				queue_test();
			} else if (!strcmp(test, "binarytree")) {
				binary_tree_test();
			} else if (!strcmp(test, "avltree")) {
				avl_tree_test();
			} else if (!strcmp(test, "priorityqueue")) {
				priority_queue_test();
			} else if (!strcmp(test, "hashtable")) {
				hash_table_test();
			} else if (!strcmp(test, "rbtree")) {
				rbtree_test();
			} else if (!strcmp(test, "rbtree2")) {
				rbtree_test2();
			} else if (!strcmp(test, "rbtree3")) {
				rbtree_test3();
			}

			break;
		default:
			break;
		}
	}

	return 0;
}
