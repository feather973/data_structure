#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include "lib/libstack.h"
#include "lib/libqueue.h"

#include "binarytree_internal.h"

static broot *g_root;
static void *g_stack;

#include "binarytree.h"
static void visit(bnode *n);

#ifndef push
#define push(n)			\
	do {				\
		n->pushed = 1;	\
		spush(g_stack, n);		\
	} while (0);
#endif

void *binit(void) {
	g_root = calloc(1, sizeof(broot));
	if (!g_root) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	g_stack = sinit();
	qinit();

	return g_root;
}

void bfinit(void *handle) {
	broot *r = handle;

	sfinit(g_stack, 1);
	qfinit(1);

	free(r);

	g_root = NULL;
}

enum {
	bnone = 0,
	bleft = 1,
	bright = 2
};

// return 0 : success
// return -1 : fail
int binsert(void *handle, int val) {
	bnode *n, *cur, *parent;
	int dir = bnone;

	broot *r = handle;

	n = calloc(1, sizeof(bnode));
	if (!n) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return -1;
	}

	if (!r->root) {
		n->val = val;
		r->root = n;
		return 0;
	}

	cur = parent = r->root;
	while (cur) {
		if (cur->val == val) {
			fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
			free(n);
			return -1;
		} else if (cur->val > val) {
			parent = cur;
			dir = bleft;

			cur = cur->left;
			continue;
		} else { 
			parent = cur;
			dir = bright;

			cur = cur->right;
			continue;
		}
	}

	n->val = val;
	n->parent = parent;
	switch (dir) {
	case bleft :
		parent->left = n;
		break;
	case bright :
		parent->right = n;
		break;
	case bnone :
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		free(n);
		return -1;
	}

	return 0;
}

// use preorder
int bdelete(void *handle, int val) {
	broot *r = handle;
	bnode *n = r->root;
	bnode *cur, *parent;		// left's rightmost cursor

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);
		
		// remove n
		if (n->val == val) {

			// if left tree exist, find left's rightmost (leaf)
			if (n->left) {
				cur = n->left;		// exist

				while (cur) {
					parent = cur;
					cur = cur->right;
				}

				n->val = parent->val;

				if (parent->parent == n)
					n->left = parent->left;
				else
					parent->parent->right = NULL;

				free(parent);
			} else if (n->right) {
				// if "no left", only right
				
				if (n == r->root) {				// root
					r->root = n->right;
				} else if (n->parent->val < n->val) {	// right
					n->parent->right = n->right;
					n->right->parent = n->parent;
				} else {								// left
					n->parent->left = n->right;
					n->right->parent = n->parent;
				}

				free(n);
			} else {
				if (n == r->root) {
					r->root = NULL;
					goto noroot;	
				} else if (n->parent->val < n->val) {		// right
					n->parent->right = NULL;
				} else {
					n->parent->left = NULL;
				}

noroot:
				free(n);
			}

			return 0;
		}

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);
	}

	// not found
	fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
	return 1;
}

static void visit(bnode *n) {
	printf("%d ", n->val);

	n->visited = 1;
}

static int isvisited(bnode *n) {
	if (n->visited)
		return 1;

	return 0;
}

static int ispushed(bnode *n) {
	if (n->pushed)
		return 1;

	return 0;
}

static void clearall(void *handle) {
	broot *r = handle;
	bnode *n = r->root;

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);
		n->pushed = 0;
		n->visited = 0;

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);
	}
}

void bpreorder(void *handle) {
	broot *r = handle;
	bnode *n = r->root;
	if (!n)
		goto end;

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);
		visit(n);

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);
	}

end:
	printf("\n");
}

// use pushed ( visited is not enough, double push can be happen )
void binorder(void *handle) {
	broot *r = handle;
	bnode *n = r->root;
	if (!n)
		goto end;

	// previous result remove
	if (ispushed(n))
		clearall(handle);

	push(n);	

	while (!sisempty(g_stack)) {
		n = spop(g_stack);

		if (n->right && !ispushed(n->right))
			push(n->right);

		if (!n->left)
			visit(n);
		else if (ispushed(n->left))
			visit(n);
		else
			push(n);

		if (n->left && !ispushed(n->left))
			push(n->left);
	}

end:
	printf("\n");
}

static int check_visitcond(bnode *n)
{
	int leftok = 0;
	int rightok = 0;

	if (n->right && isvisited(n->right))
		rightok = 1;
	else if (!n->right)
		rightok = 1;

	if (n->left && isvisited(n->left))
		leftok = 1;
	else if (!n->left)
		leftok = 1;

	return (leftok == 1 && rightok == 1) ? 1 : 0;
}

// use both pused and visited
void bpostorder(void *handle) {
	broot *r = handle;
	bnode *n = r->root;
	if (!n)
		goto end;
	
	// previous result remove
	if (ispushed(n))
		clearall(handle);
	
	push(n);

	while (!sisempty(g_stack)) {
		n = spop(g_stack);

		if (check_visitcond(n)) {
			visit(n);
		} else {
			push(n);

			if (n->right && !ispushed(n->right))
				push(n->right);

			if (n->left && !ispushed(n->left))
				push(n->left);
		}
	}

end:
	printf("\n");
}

// use queue
void blevelorder(void *handle) {
	broot *r = handle;
	bnode *n = r->root;
	if (!n)
		goto end;

	qput(n);

	while (!qisempty()) {
		n = qget();
		visit(n);

		if (n->left)
			qput(n->left);

		if (n->right)
			qput(n->right);
	}

end:
	printf("\n");
}
