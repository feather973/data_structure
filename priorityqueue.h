#ifndef __PRIORITYQUEUE_H__
#define __PRIORITYQUEUE_H__

void *pinit(void);
void pfinit(void *handle);
void pput(void *handle, int val);
int pget(void *handle);

#endif
