// priorityqueue implemented by binary heap
// - maxheap
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "lib/libstack.h"

#include "priorityqueue.h"

typedef struct _pnode {
	int val;
	struct _pnode *left;
	struct _pnode *right;
	struct _pnode *parent;
	int visited;				// pfinit (post traverse)
	int dir;
} pnode;

typedef struct _proot {
	struct _pnode *root;

	void *dirstack;				// findlastnode stack
	void *cleanstack;			// pfinit stack

	char *savedlink;			// findnextpnode array
	int savedlink_max;

	int total;
} proot;

// findnextpnode link value
#define STOP		0
#define ROOT		1
#define L			2
#define R			3

#define DEFAULT_MAX_LINK	1

// findlastnode direction stack entry
// - use remainder as direction
typedef struct _dirnode {
	int dir;
} dirnode;

// binary heap(tree) direction value
enum {
	pnone = 0,
	pleft,
	pright
};

void *pinit(void) {
	proot *r = calloc(1, sizeof(proot));
	if (!r) {
		fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
		goto err;
	}

	r->savedlink_max = DEFAULT_MAX_LINK;
	r->savedlink = calloc(1, r->savedlink_max);
	if (!r->savedlink) {
		fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);

		goto savedlink_err;
	}

	r->dirstack = sinit();
	if (!r->dirstack) {
		fprintf(stdout, "[%s(%d)] direction stack init fail\n", __func__ , __LINE__);
		goto dirstack_err;
	}

	r->cleanstack = sinit();
	if (!r->cleanstack) {
		fprintf(stdout, "[%s(%d)] cleanup stack init fail\n", __func__ , __LINE__);
		goto cleanstack_err;
	}

	r->total = 0;

	// OK
	return r;

	// FAIL
cleanstack_err:
	free(r->dirstack);
dirstack_err:
	free(r->savedlink);	
savedlink_err:
	free(r->root);
err:
	return NULL;
}

void pfinit(void *handle) {
	proot *r = (proot *) handle;
	pnode *n;

	n = r->root;
	if (!n)
		goto end;

	// postorder traverse
	spush(r->cleanstack, n);
	while (!sisempty(r->cleanstack)) {
		n = spop(r->cleanstack);

		if (!n->visited) {
			if (n->right)
				spush(r->cleanstack, n->right);
			if (n->left)
				spush(r->cleanstack, n->left);

			n->visited = 1;

			spush(r->cleanstack, n);
		} else {
			free(n);
		}
	}

end:
	free(r->savedlink);
	sfinit(r->cleanstack, 1);
	sfinit(r->dirstack, 1);

	free(r->root);
	free(r);
}

/*
 * since r->total % 2 is the link of last node
 * - 0 : left
 * - 1 : right
 * but it needs stack to access reverse order
 */
static pnode *findlastnode(void *handle) {
	proot *r = (proot *) handle;
	pnode *ret;

	int n, remainder;
	dirnode *d;

	if (!r->root)
		return NULL;

	n = r->total;
	ret = r->root;
	
	// stack direction information
	while (n > 1) {			// n == 1, terminate
		d = calloc(1, sizeof(dirnode));
		if (!d) {
			fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
			return NULL;
		}

		remainder = n % 2;
		d->dir = remainder;		// dir is "remainder"
		spush(r->dirstack, d);

		n /= 2;
	}

	// find last node
	while (!sisempty(r->dirstack)) {
		d = spop(r->dirstack);

		// remainder exist : right
		if (d->dir)
			ret = ret->right;
		else
			ret = ret->left;
	}

	return ret;
}

static void print_link(void *handle) {
	proot *r = (proot *) handle;
	int i;

	printf(" rule : ");
	for (i=0;i<r->savedlink_max;i++) {
		switch (r->savedlink[i]) {
		case L:
			printf("L ");
			break;
		case R:
			printf("R ");
			break;
		case STOP:
			goto end;
		case ROOT:
			printf("root ");
			goto end;
			break;
		default:
			printf("? ");
			break;
		}
	}

end:
	printf("\n");
}

/*
 * since binary heap is complete binary tree, 
 * getting next node's parent is predictable 
 *
 * n : 1 2 3 4 5 6 7  8  9  10 11 12 13 14 15  16
 *     . . L L R R LL LL LR LR RL RL RR RR LLL LLL
 *
 * if L, change to R
 * if R, change to L, left Rs change till meet L
 *
 * ex) LRRRRLRRR --> LRRRRRLLL
 */
static pnode *findnextpnode(void *handle) {
	proot *r = (proot *) handle;
	pnode *n;		// return node

	int i;
	char link;

	if (!r->root) {
		fprintf(stdout, "[%s(%d)] root is NULL\n", __func__ , __LINE__);
		return NULL;
	}

	n = r->root;

	for (i=0;i<r->savedlink_max;i++) {
		link = r->savedlink[i];

		if (link == L)
			n = n->left;
		else if (link == R)
			n = n->right;
		else if (link == ROOT)
			break;
	}

	/* assert */
	if (!n) {
		fprintf(stdout, "[%s(%d)] find next node found NULL\n", __func__ , __LINE__);
		return NULL;
	}

	/* debug */
	print_link(r);

	return n;
}

/*
 * swapnode
 * - since swap is not just change 2 nodes
 *   it actually change 3 links
 *
 *   n -- n->parent      =>  m -- n->parent
 *   n -- m              =>  m -- n
 *   m -- m's child      =>  n -- m's child
 *
 * - prerequisite
 *     n is m's parent
 */
static void swapnode(pnode *n, pnode *m) {
	pnode tmp;
	
	if (!n || !m)
		return;

	tmp.left = m->left;
	tmp.right = m->right;
	tmp.parent = m->parent;
	tmp.dir = m->dir;

	// n -- n->parent      =>  m -- n->parent
	m->parent = n->parent;
	if (n->parent) {
		if (n->dir == pleft) {
			n->parent->left = m;
			m->dir = pleft;
		} else if (n->dir == pright) {
			n->parent->right = m;
			m->dir = pright;
		} else if (n->dir == pnone) {		// n is root
			m->dir = pnone;
		} else {
			fprintf(stdout, "[%s(%d)] bug, n node has weird dir\n", __func__ , __LINE__);
			return;
		}
	}

	// n -- m              =>  m -- n
	if (tmp.dir == pleft) {
		m->left = n;
		m->right = n->right;
		if (n->right)		// check child update
			n->right->parent = m;

		n->parent = m;
		n->dir = pleft;	
	} else if (tmp.dir == pright) {
		m->right = n;
		m->left = n->left;
		if (n->left)		// check child update
			n->left->parent = m;

		n->parent = m;
		n->dir = pright;
	} else {
		fprintf(stdout, "[%s(%d)] m cannot be root node\n", __func__ , __LINE__);
		return;
	}

	// m -- m's child      =>  n -- m's child
	n->left = tmp.left;
	if (tmp.left)
		tmp.left->parent = n;
	n->right = tmp.right;
	if (tmp.right)
		tmp.right->parent = n;
}

/* 
 * synonyms
 * - heapify up, bubble up, perlocate up,
 *   swim up, sift up, trickle up, cascade up
 */
static void upheap(proot *r, pnode *n) {
	pnode *cur, *next;
	pnode tmp;

	cur = next = n;
	while (1) {
		next = cur->parent;

		if (next) {
			if (cur->val > next->val) {
				swapnode(next, cur);

				if (r->root == next)
					r->root = cur;
			} else
				break;
		} else
			break;
	}

	return;
}

/* 
 * synonyms
 * - heapify down, bubble down, perlocate down,
 *   sink down, sift down, trickle down, cascade down
 */
static void downheap(proot *r, pnode *n) {
	pnode *cur, *next;

	cur = next = n;
	while (next) {
		next = NULL;

		if (cur->left && cur->left->val > cur->val)
			next = cur->left;
		
		if (next) {
			if (cur->right && cur->right->val > next->val)
				next = cur->right;
		} else {
			if (cur->right && cur->right->val > cur->val)
				next = cur->right;
		}

		if (next) {
			if (!cur->parent)
				r->root = next;

			swapnode(cur, next);
		}
	}
	return;
}

void pput(void *handle, int val) {
	proot *r;
	pnode *n, *next;

	int idx;
	char *newlink;

	printf("--------\n");
	n = calloc(1, sizeof(pnode));
	if (!n) {
		fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
		return;
	}

	printf("[pput] %d\n", val);

	r = (proot *) handle;
	if (!r->root) {
		/* debug */
		print_link(r);			// no rule exist

		n->val = val;
		r->root = n;

		r->total++;

		r->savedlink[0] = ROOT;
		return;
	}

	// find next parent node
	next = findnextpnode(r);
	if (!next) {
		fprintf(stdout, "[%s(%d)] find next parent node fail\n", __func__ , __LINE__);
		return;
	}

	n->val = val;
	n->parent = next;
	if (!next->left) {
		next->left = n;
		n->dir = pleft;
	} else if (!next->right) {
		next->right = n;
		n->dir = pright;
	} else {
		fprintf(stdout, "[%s(%d)] wrong 'next parent node'\n", __func__ , __LINE__);
		return;
	}

	// update r->savedlink
	if (r->total % 2 == 0) {
		idx = r->savedlink_max - 1;

		while (idx >= 0) {
			if (r->savedlink[idx] == L) {
				r->savedlink[idx] = R;
				break;
			} else if (r->savedlink[idx] == R) {
				r->savedlink[idx] = L;
			} else if (r->savedlink[idx] == ROOT) {
				r->savedlink[idx] = L;
				break;
			}

			idx--;
		}

		// RRRR --> LLLL + L
		if (idx < 0 && r->savedlink[0] == L) {
			r->savedlink_max++;	
			newlink = calloc(1, r->savedlink_max);
			if (!newlink) {
				fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
				return;
			}

			memset(newlink, L, r->savedlink_max);
			free(r->savedlink);
			r->savedlink = newlink;
		}
	}

	// do upheap
	upheap(r, n);

	r->total++;
}

/* extract max */
int pget(void *handle) {
	proot *r;
	pnode *last;
	int ret;

	r = (proot *) handle;
	if (!r->root) {
		fprintf(stdout, "priority queue empty\n");
		return -1;
	}

	// return value
	ret = r->root->val;
	
	// find last node
	last = findlastnode(r);
	if (!last) {
		fprintf(stdout, "[%s(%d)] find last node fail\n", __func__ , __LINE__);
		return -1;
	}

	// last is root node
	if (last == r->root) {
		free(r->root);
		r->root = NULL;
		goto end; 
	}

	// last is non-root node
	if (last != r->root->left)
		last->left = r->root->left;
	else
		last->left = NULL;
	if (last != r->root->right)
		last->right = r->root->right;
	else
		last->right = NULL;

	if (last->dir == pleft) {
		last->parent->left = NULL;
	} else if (last->dir == pright) {
		last->parent->right = NULL;
	}

	// last become root
	last->dir = pnone;
	last->parent = NULL;

	if (last->left)
		last->left->parent = last;
	if (last->right)
		last->right->parent = last;

	free(r->root);
	r->root = last;

	// do downheap
	downheap(r, r->root);

end:
	r->total--;
	return ret;
}
