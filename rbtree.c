#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "rbtree.h"
#include "lib/libstack.h"
#include "lib/libqueue.h"

typedef struct _rbnode {
	int val;
	int color;
	struct _rbnode *left;
	struct _rbnode *right;
	int pushed;
	int visited;
	int dir;
	struct _rbnode *parent;
} rbnode;

typedef struct _rbroot {
	rbnode *root;
} rbroot;

#ifndef push
#define push(n)			\
	do {				\
		n->pushed = 1;	\
		spush(g_stack, n);		\
	} while (0);
#endif

rbroot *g_root;
static void *g_stack;

void *rbinit(void) {
	g_root = calloc(1, sizeof(rbroot));
	if (!g_root) {
		fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	g_stack = sinit();
	qinit();

	return g_root;
}

void rbfinit(void *handle) {
	rbroot *r = handle;

	sfinit(g_stack, 0);
	qfinit(1);

	free(r);

	g_root = NULL;

	return;
}

enum {
	NODIR= 0,
	LEFT = 1,
	RIGHT = 2
};

enum {
	NOCOLOR = 0,
	RED = 1,
	BLACK = 2,
};

enum {
	NOOP = 0,
	INSERT = 1,
	DELETE = 2,
};

// n is p->right
static void rotateleft(rbnode *n) {
	rbnode *p, *lchild, *g = NULL;		// after n->p, check lchild and g
	int dir;

	if (n->dir != RIGHT) {
		fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "n is not RIGHT");
		return;
	}

	p = n->parent;
	dir = p->dir;			// save
	lchild = n->left;

	if (p)
		g = p->parent;

	// rotate
	n->left = p;
	p->parent = n;
	p->dir = LEFT;

	p->right = lchild;
	if (lchild) {
		lchild->parent = p;
		lchild->dir = RIGHT;
	}
	if (g) {
		if (dir == LEFT)
			g->left = n;
		else if (dir == RIGHT)
			g->right = n;
		else {
			fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unknown dir");
			return;
		}
	}
	n->parent = g;
	n->dir = dir;				// use saved dir

	// new root n
	if (p == g_root->root)
		g_root->root = n;
}

// n is p->left
static void rotateright(rbnode *n) {
	rbnode *p, *rchild, *g = NULL;		// after n->p, check rchild and g
	int dir;

	if (n->dir != LEFT) {
		fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "n is not LEFT");
		return;
	}

	p = n->parent;
	dir = p->dir;			// save
	rchild = n->right;

	if (p)
		g = p->parent;

	// rotate
	n->right = p;
	p->parent = n;
	p->dir = RIGHT;

	p->left = rchild;
	if (rchild) {
		rchild->parent = p;
		rchild->dir = LEFT;
	}
	if (g) {
		if (dir == LEFT)
			g->left = n;
		else if (dir == RIGHT)
			g->right = n;
		else {
			fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unknown dir");
			return;
		}
	}
	n->parent = g;
	n->dir = dir;			// use saved dir

	// new root n
	if (p == g_root->root)
		g_root->root = n;
}

static void insert_rebalance(rbnode *n) {
	rbnode *p = NULL, *g = NULL, *u = NULL;		// insert

retry:
	if (!n) {
		fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "NULL node");
		return;
	}
	p = n->parent;

	if (p)
		g = p->parent;

	// node is root node
	if (!p) {
		n->color = BLACK;
		return;
	}

	// parent node is root node and red
	if (g_root->root == p && p->color == RED) {
		p->color = BLACK;
		return;
	}

	if (g) {
		if (p->dir == LEFT)
			u = g->right;
		else if (p->dir == RIGHT)
			u = g->left;
	}

	// parent is black node, no need to rebalance
	if (p->color == BLACK)
		return;

	// now parent is red, so grandparent is black, check uncle's color

	// 1) uncle is black
	if (!u || (u && (u->color == BLACK))) {
		// n is inner child, rotate n->p and rotate n->g
		// G is RED and n is BLACK
		if (n->dir == RIGHT && p->dir == LEFT) {
			rotateleft(n);
			rotateright(n);

			g->color = RED;
			n->color = BLACK;
			return;
		} else if (n->dir == LEFT && p->dir == RIGHT) {
			rotateright(n);
			rotateleft(n);

			g->color = RED;
			n->color = BLACK;
			return;
		}

		// n is outer child, rotate p->g
		// G is RED and P is BLACK
		if (n->dir == LEFT && p->dir == LEFT) {
			rotateright(p);

			g->color = RED;
			p->color = BLACK;

			return;
		} else if (n->dir == RIGHT && p->dir == RIGHT) {
			rotateleft(p);

			g->color = RED;
			p->color = BLACK;
			return;
		}

		// unreachable
		fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unreachable");
		return;
	}

	// 2) uncle is red
	if (u->color == RED) {
		p->color = BLACK;
		u->color = BLACK;

		g->color = RED;

		n = g;
		goto retry;		// g is red, rebalance retry need
	}

	// unreachable
	fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unreachable");
	return;
}

int rbinsert(void *handle, int val) {
	rbnode *n, *p, *cur;
	rbroot *r = handle;
	int dir;

	n = calloc(1, sizeof(rbnode));
	if (!n) {
		fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
		return -1;
	}

	if (!r->root) {
		n->val = val;
		n->parent = NULL;
		n->color = BLACK;
		n->dir = NODIR;

		r->root = n;
		return 0;
	}

	// binary search to find leaf node
	cur = p = r->root;
	while (cur) {
		if (cur->val == val) {
			fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
			free(n);
			return -1;
		} else if (cur->val > val) {
			p = cur;
			cur = cur->left;
			dir = LEFT;
		} else if (cur->val < val) {
			p = cur;
			cur = cur->right;
			dir = RIGHT;
		}
	}

	n->val = val;
	n->parent = p;
	n->color = RED;

	if (dir == LEFT)
		p->left = n;
	else if (dir == RIGHT)
		p->right = n;
	else {
		fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
		free(n);

		return -1;
	}

	n->dir = dir;

	insert_rebalance(n);
	return 0;
}

// post order removal
void removeall(void *handle) {
	rbroot *r = handle;
	rbnode *n = r->root;

	if (!n)
		return;

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);

		free(n);
	}
}

typedef struct delete_rebalance_args {
	rbnode *p;
	rbnode *s;
	rbnode *c;
	rbnode *d;
} del_args;
 
// save delete_rebalance arguments
static void save_rebal_args(rbnode *n, del_args *args) {
	rbnode *p, *s = NULL, *c = NULL, *d = NULL;
	
	p = n->parent;
	if (p) {
		// n is not root

		s = (n->dir == LEFT) ? p->right : p->left;
		if (s)
			c = (n->dir == LEFT) ? s->right : s->left;
		if (s)
			d = (n->dir == LEFT) ? s->left : s->right;
	}

	args->p = p;
	args->s = s;
	args->c = c;
	args->d = d;
}

// replacement is always leaf node
static rbnode *find_replacement(rbnode *n) {
	rbnode *cur, *rep;

	// right subtree's left most node
	if (n->right) {
		rep = n->right;
		cur = n->right->left;

		while (cur) {
			rep = cur;
			cur = cur->left;
		}

		if (rep)
			return rep;
	}

	// left subtree's right most node
	if (n->left) {
		rep = n->left;
		cur = n->left->right;

		while (cur) {
			rep = cur;
			cur = cur->right;
		}

		if (rep)
			return rep;
	}

	if (n->right)
		return n->right;

	if (n->left)
		return n->left;

	// no child
	return NULL;
}

static void delete_rebalance(del_args *args) {
	rbnode *p = args->p, *s = args->s, *c = args->c, *d = args->d;

	int color;
case1:
	if (!p)
		return;

	// check if all black
	if ((p->color == BLACK) && 
			(!s || s->color == BLACK) && (!c || c->color == BLACK) && (!d || d->color == BLACK)) {
		if (s)
			s->color = RED;

		// p is new n, update p s c d
		if (p->parent) {
			if (p->dir == LEFT)
				s = p->parent->right;
			else if (p->dir == RIGHT)
				s = p->parent->left;
		}

		if (s) {
			if (p->dir == LEFT) {
				c = s->left;
				d = s->right;
			}
			else if (p->dir == RIGHT) {
				c = s->right;
				d = s->left;
			}
		}

		p = p->parent;
		goto case1;
	}

case2:
	if ((p->color == BLACK) &&
			(s && s->color == RED) && (!c || c->color == BLACK) && (!d || d->color == BLACK)) {
		if (s->dir == LEFT)
			rotateright(s);
		else if (s->dir == RIGHT)
			rotateleft(s);
		else
			fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unknown s->dir");

		color = p->color;
		p->color = s->color;
		s->color = color;
		
		// update s c d
		s = c;
		if (s) {
			if (c->dir == LEFT) {			// deleted node's dir is not changed, so use previous c->dir
				c = s->left;
				d = s->right;
			} else if (c->dir == RIGHT) {
				c = s->right;
				d = s->left;
			}
		} else {
			c = NULL;
			d = NULL;
		}
	}

case2_1:
	if ((p->color == RED) &&
			(!s || s->color == BLACK) && (!c || c->color == BLACK) && (!d || d->color ==BLACK)) {
		color = p->color;

		if (s)
			p->color = s->color;
		else
			p->color = BLACK;

		if (s)
			s->color = color;

		return;
	}

case2_2:
	if ((!s || s->color == BLACK) && (c && c->color == RED) && (!d || d->color == BLACK)) {
		if (c->dir == LEFT)
			rotateright(c);
		else if (c->dir == RIGHT)
			rotateleft(c);
		else
			fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unknown c->dir");

		if (s)
			color = s->color;
		else
			color = BLACK;

		if (s)
			s->color = (c) ? c->color : BLACK;

		if (c)
			c->color = color;

		goto case2_3;
	}

case2_3:
	if ((s && s->color == BLACK) && (d && d->color == RED)) {		// s should be !NULL
		if (s->dir == LEFT)	
			rotateright(s);
		else if (s->dir == RIGHT)
			rotateleft(s);
		else
			fprintf(stdout, "[%s(%d)] %s\n", __func__ , __LINE__, "unknown s->dir\n");

		color = s->color;
		s->color = d->color;
		d->color = color;

		p->color = s->color;
	}

	return;
}

int rbdelete(void *handle, int val) {
	rbroot *r = handle;
	rbnode *n = r->root;
	rbnode *rep;

	del_args args = {};

	int color;			// for delete_rebalance
	int tmp;			// for switch value 

	// find node to remove
	while (n) {
		if (val > n->val) {
			n = n->right;
			continue;
		} else if (val < n->val) {
			n = n->left;
			continue;
		}

		break;	
	}

	if (!n) {
		fprintf(stdout, "[%s(%d)] %s %d\n", __func__ , __LINE__, "cannot find value", val);
		return -1;
	}

	printf("[test] remove %d\n", n->val);

retry:
	// find replacement
	rep = find_replacement(n);
	if (rep) {
		// free rep instead
		tmp = n->val;
		n->val = rep->val;
		rep->val = tmp;

		// rep has child, one more
		if (rep->left || rep->right) {
			n = rep;
			goto retry;
		}

		save_rebal_args(rep, &args);

		// before delete, remove remain link
		if (rep->dir == LEFT)
			rep->parent->left = NULL;
		else if (rep->dir == RIGHT)
			rep->parent->right = NULL;

		color = rep->color;
		free(rep);
	} else {
		// n is already leaf node

		save_rebal_args(n, &args);
		
		// before delete, remove remain link
		if (n->dir == LEFT)
			args.p->left = NULL;
		else if (n->dir == RIGHT)
			args.p->right = NULL;	
		else
			g_root->root = NULL;

		color = n->color;
		free(n);
	}

	if (color == RED)
		return 0;

	// rebalance
	delete_rebalance(&args);
	return 0;
}

#if 0
void rbpreorder(void *handle);
void rbinorder(void *handle);
void rbpostorder(void *handle);
void rblevelorder(void *handle);
#endif
