.phony: all clean

stack := lib/libstack.so
queue := lib/libqueue.so

all: circular $(stack) arrayqueue $(queue) binarytree avl priorityqueue hashtable rbtree
	gcc -o ds main.c circular.o $(stack) arrayqueue.o $(queue) binarytree.o avl.o priorityqueue.o hashtable.o rbtree.o -g

circular:
	gcc -c circular.c -g
$(stack): lib/libstack.c
	gcc --shared -o $@ $< -g -fPIC
arrayqueue:
	gcc -c arrayqueue.c -g
$(queue): lib/libqueue.c
	gcc --shared -o $@ $< -g -fPIC
binarytree:
	gcc -c binarytree.c -g
avl:
	gcc -c avl.c -g
priorityqueue:
	gcc -c priorityqueue.c -g
hashtable:
	gcc -c hashtable.c -g
rbtree:
	gcc -c rbtree.c -g
clean:
	rm -f ds circular.o arrayqueue.o lqueue.o binarytree.o avl.o priorityqueue.o hashtable.o rbtree.o
	rm -f lib/*.so
