#ifndef __BINARYTREE_INTERNAL_H__
#define __BINARYTREE_INTERNAL_H__

// binary tree (order <= 2)
// duplicate not allowed
typedef struct _bnode {
	int val;
	struct _bnode *left;
	struct _bnode *right;
	int pushed;
	int visited;
	struct _bnode *parent;
} bnode;

typedef struct _broot {
	bnode *root;
} broot;

#endif
