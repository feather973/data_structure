/*
 * copied from binarytree.c
 */

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include "lib/libstack.h"
#include "lib/libqueue.h"

/* height : height if this node is root node */
typedef struct _anode {
	int val;
	int height;			/* better than balance */
	struct _anode *left;
	struct _anode *right;
	int pushed;
	int visited;
	struct _anode *parent;
	int dir;
} anode;

typedef struct _aroot {
	anode *root;	
} aroot;

static aroot *g_root;
static void *g_stack;

#include "avl.h"
static void visit(anode *n);

#ifndef push
#define push(n)			\
	do {				\
		n->pushed = 1;	\
		spush(g_stack, n);		\
	} while (0);
#endif

void *ainit(void) {
	g_root = calloc(1, sizeof(aroot));
	if (!g_root) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	g_stack = sinit();
	qinit();

	return g_root;
}

void afinit(void *handle) {
	aroot *r = handle;

	sfinit(g_stack, 0);
	qfinit(1);

	free(r);

	g_root = NULL;
}

enum {
	anone = 0,
	aleft = 1,
	aright = 2
};

enum {
	NO_ROTATE = 0,
	LL,
	RR,
	LR,
	RL
};

static int isrotate(anode *g, anode *p) {
	int rot = NO_ROTATE;
	int g_bal, p_bal;		// check left heavy or right heavy
	int g_right_height, g_left_height, p_right_height, p_left_height;

	if (!g || !p) {
		return rot;
	}

	if (!g->left)
		g_left_height = 0;
	else 
		g_left_height = g->left->height;

	if (!g->right)
		g_right_height = 0;
	else
		g_right_height = g->right->height;

	if (!p->left)
		p_left_height = 0;
	else
		p_left_height = p->left->height;

	if (!p->right)
		p_right_height = 0;
	else
		p_right_height = p->right->height;

	g_bal = g_left_height - g_right_height;
	p_bal = p_left_height - p_right_height;

	if (abs(g_bal) <= 1)
		return rot;

	if (p_bal <= 0 && g_bal < 0)			// right heavy(or balance), right heavy : LL
		rot = LL;
	else if (p_bal >= 0 && g_bal > 0)		// left heavy(or balance), left heavy : RR
		rot = RR;
	else if (p_bal <= 0 && g_bal > 0)		// right heavy(or balance), left heavy : LR	
		rot = LR;
	else if (p_bal >= 0 && g_bal < 0)		// left heavy(or balance), right heavy : RL
		rot = RL;
	else {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
	}

	return rot;
}

static void rotate(int rot, anode *g, anode *p, anode *n) {
	anode *big = g->parent;
	anode *tmp = g;

	// assert
	if (!g || !p || !n) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return;
	}

	switch (rot) {
	case RR:
		if (g->parent) {
			if (g->dir == aleft) {
				g->parent->left = p;
				p->dir = aleft;
			} else if (g->dir == aright) {
				g->parent->right = p;
				p->dir = aright;	
			} else
				fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		}

		g->dir = aright;

		g->left = p->right;
		if (p->right) {
			p->right->parent = g;		// p->right->parent first
			p->right->dir = aleft;
		}

		p->right = g;

		if (g == g_root->root) {
			g_root->root = p;
			p->parent = NULL;
		} else
			p->parent = g->parent;

		g->parent = p;

		// height
		g->height -= 2;

		break;
	case LL:
		if (g->parent) {
			if (g->dir == aleft) {
				g->parent->left = p;
				p->dir = aleft;
			} else if (g->dir == aright) {
				g->parent->right = p;
				p->dir = aright;
			} else
				fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		}

		g->dir = aleft;

		g->right = p->left;
		if (p->left) {
			p->left->parent = g;		// p->left->parent first
			p->left->dir = aright;
		}

		p->left = g;

		if (g == g_root->root) {
			g_root->root = p;
			p->parent = NULL;
		} else
			p->parent = g->parent;

		g->parent = p;

		// height
		g->height -= 2;

		break;
	case LR:
		// L
		g->left = n;
		n->dir = aleft;

		p->right = n->left;
		if (n->left) {
			n->left->parent = p;		// n->left->parent first
			n->left->dir = aright;
		}

		n->left = p;
		n->parent = g;

		p->parent = n;

		// R
		if (g->parent) {
			if (g->dir == aleft) {
				g->parent->left = n;
				n->dir = aleft;
			} else if (g->dir == aright) {
				g->parent->right = n;
				n->dir = aright;
			} else 
				fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		}

		g->dir = aright;

		g->left = n->right;
		if (n->right) {
			n->right->parent = g;
			n->right->dir = aleft;
		}

		n->right = g;

		if (g == g_root->root) {
			g_root->root = n;
			n->parent = NULL;
		} else
			n->parent = g->parent;

		g->parent = n;

		// height 
		g->height -= 2;
		p->height -= 1;
		n->height += 1;

		break;
	case RL:
		// R
		g->right = n;
		n->dir = aright;

		p->left = n->right;
		if (n->right) {
			n->right->parent = p;		// n->right->parent first
			n->right->dir = aleft;
		}

		n->right = p;
		n->parent = g;

		p->parent = n;

		// L
		if (g->parent) {
			if (g->dir == aleft) {
				g->parent->left = n;
				n->dir = aleft;
			} else if (g->dir == aright) {
				g->parent->right = n;	
				n->dir = aright;
			} else 
				fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		}

		g->dir = aleft;

		g->right = n->left;
		if (n->left) {
			n->left->parent = g;
			n->left->dir = aright;
		}

		n->left = g;

		if (g == g_root->root) {
			g_root->root = n;
			n->parent = NULL;
		} else
			n->parent = g->parent;

		g->parent = n;

		// height
		g->height -= 2;
		p->height -= 1;
		n->height += 1;

		break;
	default:
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		break;
	}
}

// return 0 : success
// return -1 : fail
int ainsert(void *handle, int val) {
	anode *n, *cur, *p, *g, *sibling;
	int dir = anone;

	aroot *r = handle;
	int rot = NO_ROTATE;
	int need_rot = 0;

	n = calloc(1, sizeof(anode));
	if (!n) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return -1;
	}

	if (!r->root) {
		n->val = val;
		n->parent = NULL;
		n->height = 1;
		n->dir = dir;

		r->root = n;
		return 0;
	}

	cur = p = r->root;
	while (cur) {
		if (cur->val == val) {
			fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
			free(n);
			return -1;
		} else if (cur->val > val) {
			p = cur;
			dir = aleft;

			cur = cur->left;
			continue;
		} else { 
			p = cur;
			dir = aright;

			cur = cur->right;
			continue;
		}
	}

	n->val = val;
	n->parent = p;
	n->dir = dir;			// use for update height
	n->height = 1;

	if (p != g_root->root && !p->left && !p->right)
		need_rot = 1;

	// check sibling and update height
	switch (dir) {
	case aleft :
		p->left = n;

		if (!p->right)
			p->height++;
		break;
	case aright :
		p->right = n;

		if (!p->left)
			p->height++;
		break;
	case anone :
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		free(n);

		return -1;
	}

	// if p is not leaf node, no rotate happen ( no level changed )
	if (!need_rot)
		goto no_rotate;

	g = p->parent;
	if (!g)
		goto no_rotate;

	while (g) {
		sibling	= (p->dir == aleft) ? g->right : g->left;

		if (sibling && p->height <= sibling->height)
			goto skip_height;

		g->height++;

skip_height:
		// check
		rot = isrotate(g, p);
		if (rot != NO_ROTATE) {
			rotate(rot, g, p, n);
			break;
		}

		n = p;
		p = g;
		g = g->parent;		// if g root, loop terminate
	}

no_rotate:
	return 0;
}

static void update_height(anode *n) {
	anode *sibling, *p;

	p = n->parent;
	if (p)
		sibling = (n->dir == aleft) ? p->right : p->left;

	while (p) {
		if (!sibling)
			goto next;

		if (n->height <= sibling->height)
			break;

next:
		p->height--;

		n = p;
		p = p->parent;

		if (p)
			sibling = (n->dir == aleft) ? p->right : p->left;
	}
}

static int __rebalance(anode *n) {
	anode *p, *g, *c;
	int rot = NO_ROTATE;

	if (!n->parent)
		return NO_ROTATE;

	g = n->parent;
	if (n->dir == aleft) {
		p = g->right;		// n sibling
	} else if (n->dir == aright) {
		p = g->left;		// n sibling
	} else {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NO_ROTATE;
	}
	
	rot = isrotate(g, p);

	switch (rot) {
	case LL: 
		c = p->right;
		break;
	case RR:
		c = p->left;
		break;
	case LR:
		c = p->right;
		break;
	case RL:
		c = p->left;
		break;
	default:
		goto no_rotate;
	}

	/*
	 * this is assertion of rotate
	 */
	if (rot != NO_ROTATE && (!g || !p || !c)) {
		if (!g)
			printf("g NULL ");
		if (!p)
			printf("p NULL");
		if (!c)
			printf("c NULL");

		printf("\n");
		exit(0);
	}
	rotate(rot, g, p, c);

no_rotate:
	return rot;
}

static void rebalance(anode *n) {
	anode *p, *sibling, *c;
	int rot;

	while (n) {
		// get p, sibling, c(LR or RL)
		p = n->parent;
		if (!p)
			break;

		if (n->dir == aleft) {
			sibling = p->right;
		} else if (n->dir == aright) {
			sibling = p->left;
		} else {
			fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
			return;
		}

		// reblanace
		rot = __rebalance(n);

		// next n
		if (rot == NO_ROTATE) {
			n = n->parent;
		} else {
			if (rot == LL || rot == RR) {
				n = sibling;
			} else if (rot == LR) {
				c = sibling->right;
				n = c;
			} else if (rot == RL) {
				c = sibling->left;
				n = c;
			}
		}
	}
}

// use preorder
int adelete(void *handle, int val) {
	aroot *r = handle;
	anode *n = r->root;
	anode *cur, *replace;		// left's rightmost cursor

	if (!n)
		return 1;

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);
		
		// remove n
		if (n->val == val) {

			// if left tree exist, find left's rightmost (leaf)
			if (n->left) {
				cur = n->left;		// exist

				while (cur) {
					replace = cur;
					cur = cur->right;
				}

				n->val = replace->val;

				if (replace->parent == n) {
					n->left = replace->left;
				} else {
					replace->parent->right = NULL;
					n->dir = aleft;				// since n->left->right..right is aright, change is needed
				}

				update_height(replace);
				rebalance(replace);

				free(replace);
			} else if (n->right) {
				// if "no left", only right
				
				if (n == r->root) {				// root
					r->root = n->right;
				} else if (n->dir == aright) {
					n->parent->right = n->right;
					n->right->parent = n->parent;
				} else if (n->dir == aleft) {
					n->parent->left = n->right;
					n->right->parent = n->parent;
					n->right->dir = aleft;		// since n->right is aright, change is needed
				} else {
					fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
					return 1;
				}
				
				/*
				 * n->right case is different, since it delete n->parent link.
				 * so update_height and rebalance cannot perform correctly.
				 *
				 * so, we use n->right instead.
				 */
				update_height(n->right);
				rebalance(n->right);

				free(n);
			} else {
				if (n == r->root) {				// root
					r->root = NULL;
					goto noroot;	
				} else if (n->dir == aright) {
					n->parent->right = NULL;
				} else if (n->dir == aleft) {
					n->parent->left = NULL;
				} else {
					fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
					return 1;
				}

				update_height(n);
				rebalance(n);

noroot:
				free(n);
			}
			
			return 0;
		}

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);
	}

	// not found
	fprintf(stdout, "[%s(%d)] %s(%d)\n", __func__ , __LINE__, strerror(errno), errno);
	return 1;
}

static void visit(anode *n) {
	printf("%d ", n->val);

	n->visited = 1;
}

static int isvisited(anode *n) {
	if (n->visited)
		return 1;

	return 0;
}

static int ispushed(anode *n) {
	if (n->pushed)
		return 1;

	return 0;
}

static void clearall(void *handle) {
	aroot *r = handle;
	anode *n = r->root;

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);
		n->pushed = 0;
		n->visited = 0;

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);
	}
}

void apreorder(void *handle) {
	aroot *r = handle;
	anode *n = r->root;
	if (!n)
		goto end;

	push(n);
	while (!sisempty(g_stack)) {
		n = spop(g_stack);
		visit(n);

		if (n->right)
			push(n->right);

		if (n->left)
			push(n->left);
	}

end:
	printf("\n");
}

// use pushed ( visited is not enough, double push can be happen )
void ainorder(void *handle) {
	aroot *r = handle;
	anode *n = r->root;
	if (!n)
		goto end;

	// previous result remove
	if (ispushed(n))
		clearall(handle);

	push(n);	

	while (!sisempty(g_stack)) {
		n = spop(g_stack);

		if (n->right && !ispushed(n->right))
			push(n->right);

		if (!n->left)
			visit(n);
		else if (ispushed(n->left))
			visit(n);
		else
			push(n);

		if (n->left && !ispushed(n->left))
			push(n->left);
	}

end:
	printf("\n");
}

static int check_visitcond(anode *n) {
	int leftok = 0;
	int rightok = 0;

	if (n->right && isvisited(n->right))
		rightok = 1;
	else if (!n->right)
		rightok = 1;

	if (n->left && isvisited(n->left))
		leftok = 1;
	else if (!n->left)
		leftok = 1;

	return (leftok == 1 && rightok == 1) ? 1 : 0;
}

// use both pushed and visited
void apostorder(void *handle) {
	aroot *r = handle;
	anode *n = r->root;
	if (!n)
		goto end;
	
	// previous result remove
	if (ispushed(n))
		clearall(handle);
	
	push(n);

	while (!sisempty(g_stack)) {
		n = spop(g_stack);

		if (check_visitcond(n)) {
			visit(n);
		} else {
			push(n);

			if (n->right && !ispushed(n->right))
				push(n->right);

			if (n->left && !ispushed(n->left))
				push(n->left);
		}
	}

end:
	printf("\n");
}

// use queue
void alevelorder(void *handle) {
	aroot *r = handle;
	anode *n = r->root;
	if (!n)
		goto end;

	qput(n);

	while (!qisempty()) {
		n = qget();
		visit(n);

		if (n->left)
			qput(n->left);

		if (n->right)
			qput(n->right);
	}

end:
	printf("\n");

}
