#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

typedef struct _snode {
	void *n;
	struct _snode *next;
} snode;

typedef struct _sroot {
	struct _snode *head;
	struct _snode *end;
} sroot;

/* head(top), end always empty */

/*
 *  ex) push a b c d e
 *	head -- e -- d -- c -- b -- a -- end
 *
 *	push and pop happens in head
 */

#include "libstack.h"

void *sinit(void) {
	sroot *root;

	root = calloc(1, sizeof(sroot));
	if (!root) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	root->head = calloc(1, sizeof(snode));
	root->end = calloc(1, sizeof(snode));
	
	if (!root->head || !root->end) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return NULL;
	}

	root->head->next = root->end;
	root->end->next = root->end;

	return root;
}

void sfinit(void *handle, int clear) {
	sroot *r = (sroot *) handle;
	snode *tmp;

	while (r->head != r->end) {
		tmp = r->head->next;
		if (clear && r->head->n)
			free(r->head->n);
		free(r->head);
		r->head = tmp;
	}

	r->head = NULL;

	free(r->end);
	r->end = NULL;	

	free(r);
}

/*
 * push a b c
 * head -- a -- end
 * head -- b -- a -- end
 * head -- c -- b -- a -- end
 */
void spush(void *handle, void *n) {
	sroot *r = (sroot *) handle;
	snode *s;

	s = calloc(1, sizeof(snode));
	if (!s) {
		fprintf(stdout, "[%s(%d)] %s(%d)", __func__ , __LINE__, strerror(errno), errno);
		return;
	}
	
	s->n = n;
	s->next = r->head->next;

	r->head->next = s;
}

/*
 * pop pop pop
 * head -- b -- a -- end
 * head -- a -- end
 * head -- end
 */
void *spop(void *handle) {
	void *ret;
	sroot *r = (sroot *) handle;
	snode *remove;

	if (r->head->next == r->end) {
		return NULL;
	}

	remove = r->head->next;
	ret = remove->n;

	r->head->next = remove->next;
	free(remove);

	return ret;
}

int sisempty(void *handle) {
	sroot *r = (sroot *) handle;

	if (r->head->next == r->end) {
		return 1;
	}

	return 0;
}
